﻿using Application.Interface;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebDDD.Controllers
{
    public class ProductController : Controller
    {
        private readonly InterfaceAppProduct appProduct;
        // GET: Product
        public ProductController(InterfaceAppProduct _appProduct)
        {
            appProduct = _appProduct;

        }
        public ActionResult Index()
        {
            return View(appProduct.ListAll());
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View(appProduct.GetById(id));
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View(new Product());
        }

        // POST: Product/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product collection)
        {
            try
            {
                // TODO: Add insert logic here
                appProduct.Add(collection);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            return View(appProduct.GetById(id));
        }

        // POST: Product/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product collection)
        {
            try
            {
                // TODO: Add insert logic here
                appProduct.Update(collection);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View(appProduct.GetById(id));
        }

        // POST: Product/Delete/5 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Product collection)
        {
            try
            {
                appProduct.Remove(collection);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}