﻿using Domain.Intefaces.Generic;
using Domain.Entities;


namespace Domain.Intefaces.Products
{
   public interface InterfaceProduct : InterfaceGeneric<Product>
    {
    }
}
