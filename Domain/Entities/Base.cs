﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Base
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
