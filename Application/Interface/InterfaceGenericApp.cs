﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interface
{
    public interface InterfaceGenericApp<T> where T: class
    {
        //Metodo Genérico para adicionar 
        void Add(T Obeject);

        //Metodo Genérico para atualizar 
        void Update(T Obeject);

        //Metodo Genérico para remover 
        void Remove(T Obeject);

        //Metodo Genérico para Listar um especifico 
        T GetById(int Id);

        //Metodo Genérico para listar todos 
        IList<T> ListAll();


    }
}
