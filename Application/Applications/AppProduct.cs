﻿using Application.Interface;
using Domain.Entities;
using Domain.Intefaces.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Applications
{
    public class AppProduct : InterfaceAppProduct
    {
        InterfaceProduct _interfaceProduct;
        public AppProduct(InterfaceProduct interfaceProduct) {
            _interfaceProduct = interfaceProduct; 
        }
        public void Add(Product Obeject)
        {
            _interfaceProduct.Add(Obeject);
        }

        public Product GetById(int Id)
        {
            return _interfaceProduct.GetById(Id);
        }

        public IList<Product> ListAll()
        {
            return _interfaceProduct.ListAll();
        }

        public void Remove(Product Obeject)
        {
            _interfaceProduct.Remove(Obeject);
        }

        public void Update(Product Obeject)
        {
            _interfaceProduct.Update(Obeject);
        }
    }
}
