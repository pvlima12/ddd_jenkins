﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Config
{
    public class Context : DbContext
    {

        public Context(DbContextOptions<Context> options) : base(options)
        {
        }
        public DbSet<Product> Product { set; get; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseNpgsql(GetStringConnectionConfig());

            base.OnConfiguring(optionsBuilder);

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        private string GetStringConnectionConfig()
        {
            DbConnect DAO = new DbConnect()
            {
                Host = "192.168.1.12",
                Database = "postgres",
                Username = "admin",
                Password = "V0xdata",
                Port = 5434
            };
            string strCon = $"Host={DAO.Host};Database={DAO.Database};Username={DAO.Username};Password={DAO.Password};Port={DAO.Port}";
            return strCon;
        }
    }

    public class DbConnect
    {
        public string Host { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
    }
}
