﻿using Domain.Intefaces.Generic;
using Infrastructure.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Repository.Generic
{
    public class RepositoryGeneric<T> : InterfaceGeneric<T>, IDisposable where T : class
    {
        private readonly DbContextOptions<Context> _optionsBuilder;
        public RepositoryGeneric()
        {
            _optionsBuilder = new DbContextOptions<Context>();
        }
        public void Add(T Obeject)
        {
            using (Context context = new Context(_optionsBuilder))
            {
                context.Set<T>().Add(Obeject);
                context.SaveChanges();

            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(true);
        }

        public T GetById(int Id)
        {
            using (Context context = new Context(_optionsBuilder))
            {
                return context.Set<T>().Find(Id);
            }
        }

        public IList<T> ListAll()
        {
            using (Context context = new Context(_optionsBuilder))
            {
                return context.Set<T>().AsNoTracking().ToList();

            }
        }

        public void Remove(T Obeject)
        {
            using (Context context = new Context(_optionsBuilder))
            {
                context.Set<T>().Remove(Obeject);
                context.SaveChanges();
            }
        }

        public void Update(T Obeject)
        {
            using (Context context = new Context(_optionsBuilder))
            {
                context.Set<T>().Update(Obeject);
                context.SaveChanges();
            }
        }
    }
}
