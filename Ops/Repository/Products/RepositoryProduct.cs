﻿using Infrastructure.Repository.Generic;
using Domain.Entities;
using Domain.Intefaces.Generic;
using Domain.Intefaces.Products;

namespace Infrastructure.Repository.Products
{
   public class RepositoryProduct : RepositoryGeneric<Product>, InterfaceProduct
    {
    }
}
